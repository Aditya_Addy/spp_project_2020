#include<bits/stdc++.h>
using namespace std;

// to store the nth key at nth place
vector<string> arr;

class kvstore{
public:
	bool isend;
	string value;
	kvstore* character[127];
	bool isparent;

	kvstore(){
		this->isend = false;
		this->isparent = false;
		this->value = "";
		for(int i=0;i<127;i++){
			this->character[i] = NULL;
		}
	} 

	bool get(string key){
		//Your Code Here
		if(this == NULL)
			return false;
		kvstore* current = this;
		for(int i=0;i<key.length();i++){
			current = current->character[key[i]];
			if(current == NULL){
				return false;
			}
		}
		return true;
		//return current->value;
	}
	string getval(string key){
		//Your Code Here
		if(this == NULL)
			return "Nopes";
		kvstore* current = this;
		for(int i=0;i<key.length();i++){
			current = current->character[key[i]];
			if(current == NULL){
				return "Nopes";
			}
		}
		//return true;
		return current->value;
	}

	bool put(string key, string value){
		//Your Code Here
		kvstore* current = this;
		for(int i=0;i<key.length();i++){
			if(current->character[key[i]]==NULL){
				current->character[key[i]] = new kvstore;
			}
			current->isparent = true;
			current = current->character[key[i]];
		}
		current->isend = true;
		current->value = value;
		arr.push_back(key);
		return true;
	}

	bool del(string key){
		//Your Code Here

		return deletion(this, key);
	}



	bool deletion(kvstore* root, string key){
		kvstore* current = root;
		if(current==NULL){
			return false;
		}
		

		if(key.length()){
			if(current != NULL && current->character[key[0]] != NULL && deletion(current->character[key[0]],key.substr(1)) && current->isend == false){
				
				if(!current->isparent){
					delete current;
					current = NULL;
					return true;
				}
				else{
					return false;
				}
			}
		}

		if(key.length() == 0 && current->isend){
			if(!current->isparent){
				delete current;
				current = NULL;
				return true;
			}
			else{
				current->isend = false;
				current->value = "";
				return false;
			}
		}
		return false;
	}

	pair<string,string> get(int N){
		// Your Code Here
		string key = arr[N];
		string value = getval(key);
		pair<string,string> temp = make_pair(key,value);
		return temp;
	}

	bool del(int N){
		// Your Code Here
		string key = arr[N];
		auto it1 = arr.begin();
		for(int i=0;i<N;i++)
			it1++;
		arr.erase(it1);
		return deletion(this,key);
		
	}
};



void display(kvstore* root, char str[], int level) 
	{	  
    if (root->isend)  
    { 
        str[level] = '\0'; 
        cout << str << endl; 
    } 
    int i; 
    for (i = 0; i < 127; i++)  
    { 
        if (root->character[i])  
        { 
            str[level] = i + 'a'; 
            display(root->character[i], str, level + 1); 
        } 
    } 
}